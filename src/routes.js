import React from 'react';
import { BrowserRouter as Router, Route,Redirect  } from "react-router-dom";
// import pages
import Login from './pages/login';
import Register from './pages/register';
import Dashboard from './pages/dashboard';
import Users from './pages/users';

// impoort layouts
import Main from './layouts/main';

export default () => {
    return (
      <Router>
          <Route exact path="/users" component={() =><Main><Users/></Main>} />
          <Route exact path="/dashboard" component={() =><Main><Dashboard/></Main>} />
          <Route exact path="/login" component={() =><Login/>} />
          <Route exact path="/register" component={() =><Register/>} />
      </Router>
    );
  }