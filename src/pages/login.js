import React, { Component, useState } from 'react';
import { Link, Redirect, withRouter } from "react-router-dom";
import { Query, Subscription, withApollo } from "react-apollo";
import { compose } from "recompose";
import { Row, Col, Form, Icon, Input, Button, notification, List, Checkbox } from 'antd';
import { makeRequest } from '../libs/api';
import { TOKEN } from '../config';
import { QUERY_USER } from '../graphql/user';

const { Item } = Form;

const Login = ({ history, form, client }) => {
  const { getFieldDecorator } = form;


  const handleSubmit = (e) => {
    e.preventDefault();
    form.validateFields(async (err, { username, password }) => {
      if (!err) {
        console.log('Received values of form: ', username);
        console.log('Received values of form: ', password);

        try {
          const { data } = await makeRequest('POST', '/login', {
            username,
            password
          });
          console.log('Received values of form: ', data);
          if (data.message == "Not Found") {
            notification.error({
              message: 'Oh oh',
              description: data.message,
            });
          } else {
            if(data.message == "Wrong password"){
              notification.error({
                message: 'Oh oh',
                description: data.message,
              });
            }else{
              localStorage.setItem(TOKEN, data.token);
              window.location = '/dashboard';
            }
          }



        } catch (e) {
          notification.error({
            message: 'Oh oh',
            description: e.message,

          });
        }
      }
    });
  }


  return (
    <>
      <Row>
        <Col offset={9} span={8}>
          <Form onSubmit={handleSubmit} style={{
            marginTop: "20vh"
          }}>
            <Item>
              <Row>
                <Col span={16}>
                  {getFieldDecorator('username', {
                    rules: [{ required: true, message: 'Please input your username!' }],
                  })(
                    <Input
                      prefix={<Icon type="user"
                        style={{ color: 'rgba(0,0,0,.25)' }} />}
                      placeholder="Username"
                    />,
                  )}
                </Col>
              </Row>
            </Item>
            <Item>
              <Row>
                <Col span={16}>
                  {getFieldDecorator('password', {
                    rules: [{ required: true, message: 'Please input your Password!' }],
                  })(
                    <Input
                      prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                      type="password"
                      placeholder="Password"
                    />,
                  )}
                </Col>
              </Row>
            </Item>
            <Item>
              <Row>
                <Col span={16}>
                  {getFieldDecorator('remember', {
                    valuePropName: 'checked',
                    initialValue: false,
                  })(<Checkbox>Remember me</Checkbox>)}

                </Col>
              </Row>
              <Row>
                <Col span={16}>
                  <Button block type="primary" htmlType="submit" className="login-form-button" >
                    Login
                  </Button>
                </Col>
              </Row>
              <Row>
                <Col span={16}>
                  <Button block type="primary" className="login-form-button">
                    <Link to='/register'>Register now!</Link>
                  </Button>
                </Col>
              </Row>
            </Item>
          </Form>
        </Col>
      </Row>
    </>
  );

}

export default compose(
  withApollo,
  withRouter,
  Form.create({ name: 'login' })
)(Login);



