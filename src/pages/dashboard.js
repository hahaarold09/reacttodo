import React, { Component, useState } from 'react';
import { Link } from "react-router-dom";
import { Query, Subscription, withApollo } from "react-apollo";
import { compose } from "recompose";
import {
  Row,
  Col,
  Form,
  Icon,
  Input,
  Button,
  notification,
  List,
  Checkbox
} from 'antd';


import { SUBSCRIPTIONS_TODOS, MUTATION_TODO, DELETE_TODO } from '../graphql/getTodos';

const { Item } = Form;

const Dashboard = ({ form, client }) => {
  const { getFieldDecorator,getFieldValue,getFieldProps } = form;

  const handleSubmit = (e) => {
    e.preventDefault();
    form.validateFields(async (err,{todo}) => {
      if (!err) {
        try {
          const { data } = await client.mutate({
            mutation: MUTATION_TODO,
            variables: {
              name: todo
            }
          });
          console.log('Received values of form: ', data);


          notification.success({
            message: 'Done creating todo',
            description: 'Good job',
          });
          form.resetFields();




        } catch (e) {
          notification.error({
            message: 'Oh no!!!',
            description: e.message,
          });
        }
      }
    });
  }
  const onhandleDeleteTodo = (id) => {
    try {
      return async () => {
        await client.mutate({
          mutation: DELETE_TODO,
          variables: {
            id: id
          }
        });
        notification.success({
          message: 'Done deleting todo',
          description: 'Good job',
        });
      }

    } catch (e) {
      notification.error({
        message: 'Oh no!!!',
        description: 'Bad job',
      });
    }
  }
  const handleChange = e => {
    form.getFieldValue('todo')
  };

  return (
    <div >
      <Form onSubmit={handleSubmit}>
        <Item>
          <Row>
            <Col
              xs={{ span: "24" }}
              sm={{ span: "24" }}
              md={{ span: "18" }}
              lg={{ span: "16" }}>
              {
                getFieldDecorator('todo', {
                  rules: [
                    {
                      required: true,
                      message: 'Please input todo!',
                    },
                  ],
                })(<Input />)
              }

            </Col>
            <Col>
            </Col>
            <Col xs={{ span: "24" }}
              sm={{ span: "24" }}
              md={{ span: "6" }}
              lg={{ span: "8" }}>
              <Button block type="primary" htmlType="submit" icon="plus">
                Create Todo
              </Button>
            </Col>
          </Row>
        </Item>
      </Form>
      <Subscription subscription={SUBSCRIPTIONS_TODOS}>
        {
          ({ data, loading }) => {
            if (loading) {
              return <div>Loading...</div>
            }
            return (
              <List
                bordered
                dataSource={data.todo}
                size="large"
                pagination={{
                  onChange: page => {
                    console.log(page);
                    console.log(data);

                  },
                  pageSize: 3,
                }}

                renderItem={item => (
                  <List.Item>
                    <div  >
                      {item.name}
                      <Icon type="delete" onClick={onhandleDeleteTodo(item.id)}></Icon>

                    </div>
                  </List.Item>
                )}
              />
            )
          }
        }
      </Subscription>
    </div>
  );

}

export default compose(
  withApollo,
  Form.create({ name: 'dashboard' })
)(Dashboard);
