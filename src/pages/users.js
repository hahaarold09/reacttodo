import React, { Component, useContext, useState, useEffect } from 'react';
import { Link } from "react-router-dom";
import { Query, Subscription, withApollo } from "react-apollo";
import { compose } from "recompose";
import {
  Row, Col, Form, Icon, Input, Button, notification,
  List, Checkbox, DatePicker, TimePicker,
  Select, Cascader, InputNumber
} from 'antd';
import CommonInput from '../components/input'
import { makeRequest } from '../libs/api';

import { SUBSCRIPTIONS_TODOS } from '../graphql/getTodos';

const { Item } = Form;

const Users = ({ form, client }) => {

  const [mode, setMode] = useState('add');
  const [userData, setUserData] = useState({
    idNo : null
  });

  const { getFieldDecorator } = form;

  const handleReset = () => {
    form.resetFields();
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    form.validateFields(async (err, { idno, lname, fname, mname, gender, bday, sss, tin, hdmf, phealth }) => {
      if (!err) {
        // console.log('Received values of form: ', idno, lastname,
        //   firstname,
        //   middlename,
        //   gender,
        //   bday,
        //   sss_num,
        //   tin_num,
        //   hdmf_num,
        //   phhealth_num);

        try {

          const { data } = await makeRequest('POST', '/add-employee', {
            idno, lname, fname, mname, gender, bday, sss, tin, hdmf, phealth
          });

          if (data.message == "Employee Added!") {
            console.log('success: ', data.message)
            form.resetFields();
            notification.success({
              message: data.message,
              description: 'Good job',
            });
          } else {
            console.log('error: ', data.message)
            notification.error({
              message: data.message,
              description: 'Bad job',
            });
          }


        } catch (e) {
          notification.error({
            message: 'Oh no!!!',
            description: 'Bad job',
          });
        }
      }
    });
  }
  const changedateformat = (birthday) => {

    return new Intl.DateTimeFormat('en-US', { year: 'numeric', month: '2-digit', day: '2-digit' }).format(birthday);

  }
  const changetimeformat = (time) => {

    return new Intl.DateTimeFormat('en-US', { hour: '2-digit', minute: '2-digit', second: '2-digit' }).format(time);

  }

  useEffect(()=>{
    setTimeout(()=>{
      setUserData({
        idNo : '123123asdds'
      })
    }, 2000);
    
  },[]);

  const { Option } = Select;

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8, pull: 2 },

    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16, pull: 2 },
    },
  };


  return (

    <div>
      <Row>
        <Form {...formItemLayout} onSubmit={handleSubmit}>
          <Col lg={{ span: "9", offset: "2" }} >
            <CommonInput label="ID no." colon={false}>
              {
                getFieldDecorator('idno', {
                  initialValue : userData.idNo,
                  rules: [
                    {
                      required: false,
                      message: 'Please input your last name!',
                    },
                  ],
                })(<Input />)
              }
            </CommonInput>
            <Item label="Last Name" colon={false}>
              {
                getFieldDecorator('lname', {
                  rules: [
                    {
                      required: false,
                      message: 'Please input your last name!',
                    },
                  ],
                })(<Input />)
              }
            </Item>
            <Item label="First Name" colon={false}>
              {
                getFieldDecorator('fname', {
                  rules: [
                    {
                      required: false,
                      message: 'Please input your first name!',
                    },
                  ],
                })(<Input />)
              }
            </Item>
            <Item label="Middle Name" colon={false}>
              {
                getFieldDecorator('mname', {
                  rules: [
                    {
                      required: false,
                      message: 'Please input your middle name!',
                    },
                  ],
                })(<Input />)
              }
            </Item>
            <Item label="Gender" colon={false}>
              {
                getFieldDecorator('gender', {
                  rules: [
                    {
                      required: false,
                      message: 'Please select your gender!',

                    },
                  ],
                })(
                  <Select >
                    <Option value="Male">Male</Option>
                    <Option value="Female">Female</Option>
                  </Select>
                )
              }

            </Item>
            <Item label="Birthday" colon={false}>
              {
                getFieldDecorator('bday', {
                  rules: [
                    {
                      required: false,
                      message: 'Please select your birthday!',
                    },
                  ],
                })(<DatePicker
                  style={{ width: '100%' }} />)
              }
            </Item>
          </Col>
          <Col lg={2} />
          <Col lg={{ span: "9" }}>
            <Item label="SSS" colon={false} >
              {
                getFieldDecorator('sss', {
                  rules: [
                    {
                      required: false,
                      message: 'Please input your SSS no.!',
                    },
                  ],
                })(<Input />)
              }
            </Item>
            <Item label="TIN" colon={false} >
              {
                getFieldDecorator('tin', {
                  rules: [
                    {
                      required: false,
                      message: 'Please input your TIN no.!',
                    },
                  ],
                })(<Input />)
              }
            </Item>
            <Item label="HDMF" colon={false} >
              {
                getFieldDecorator('hdmf', {
                  rules: [
                    {
                      required: false,
                      message: 'Please input your HDMF no.!',
                    },
                  ],
                })(<Input />)
              }
            </Item>
            <Item label="Phil Health" colon={false} >
              {
                getFieldDecorator('phealth', {
                  rules: [
                    {
                      required: false,
                      message: 'Please input your PHIL Health no.!',
                    },
                  ],
                })(<Input />)
              }
            </Item>
          </Col>
        </Form>
      </Row>

      <Row>
        <Col lg={{ span: "10", offset: "2" }} >
          <Button block type="primary" htmlType="submit"> Save</Button>
        </Col>
        <Col lg={1} >
        </Col>
        <Col lg={{ span: "10" }}>
          <Button block type="danger" onClick={handleReset}> Cancel</Button>
        </Col>
      </Row>
    </div>



  );

}

export default compose(
  withApollo,
  Form.create({ name: 'users' })
)(Users);



