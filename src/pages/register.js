import React, { useState } from 'react';
import { Link, Redirect, withRouter } from "react-router-dom";
import { Query, Subscription, withApollo } from "react-apollo";
import { compose } from "recompose";
import {
    Row,
    Col,
    Form,
    Icon,
    Input,
    Button,
    notification,
    List,
    Checkbox
} from 'antd';
import { makeRequest } from '../libs/api';
import { MUTATION_USER, CHECK_USER } from '../graphql/user';

const { Item } = Form;

const Register = ({ history, form, client }) => {
    const { getFieldDecorator } = form;

    const [comfirmDirty, setComfirmDirty] = useState(false);

    const validateToNextPassword = (rule, value, callback) => {
        if (value && comfirmDirty) {
            form.validateFields(['confirm'], { force: true });
        }
        callback();
    };

    const compareToFirstPassword = (rule, value, callback) => {
        if (value && value !== form.getFieldValue('password')) {
            callback('Two passwords that you enter is inconsistent!');
        } else {
            callback();
        }
    };

    const handleConfirmBlur = e => {
        console.log(comfirmDirty || !!e.target.value);
        setComfirmDirty({ confirmDirty: comfirmDirty || !!e.target.value });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        form.validateFields(async (err, { username, password, email }) => {
            if (!err) {
                console.log('Received values of form: ', username, password);
                try {
                    await makeRequest('POST', '/register', {
                        username,
                        password,
                        email
                    });
                    notification.success({
                        message: 'Create Account Success!',
                        description: 'Good job',
                    });
                    history.push('/login');

                } catch (e) {
                    console.log(e);
                    notification.error({
                        message: 'Oh no!!!',
                        description: 'Bad job',
                    });
                }
            }
        });
    }
    return (
        <Row>
            <Col offset={8} span={8}>
                <Form onSubmit={handleSubmit}>
                    <Item label="E-mail">
                        {
                            getFieldDecorator('email', {
                                rules: [
                                    {
                                        type: 'email',
                                        message: 'The input is not valid E-mail!',
                                    },
                                    {
                                        required: true,
                                        message: 'Please input your E-mail!',
                                    },
                                ],
                            })(<Input />)
                        }
                    </Item>
                    <Item label="Username">
                        {
                            getFieldDecorator('username', {
                                rules: [
                                    {
                                        required: true,
                                        message: 'Please input your username!',
                                    },
                                ],
                            })(<Input />)
                        }
                    </Item>
                    <Item label="Password" hasFeedback>
                        {getFieldDecorator('password', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Please input your password!',
                                },
                                {
                                    validator: validateToNextPassword,
                                },
                            ],
                        })(<Input.Password />)}
                    </Item>
                    <Item label="Confirm Password" hasFeedback>
                        {getFieldDecorator('confirm', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Please confirm your password!',
                                },
                                {
                                    validator: compareToFirstPassword,
                                },
                            ],
                        })(<Input.Password onBlur={handleConfirmBlur} />)}
                    </Item>
                    <Item>
                        <Button block type="primary" htmlType="submit">
                            Submit
                            </Button>
                    </Item>
                </Form>
            </Col>
        </Row>
    );

}

export default compose(
    withApollo,
    withRouter,
    Form.create({ name: 'register' })
)(Register);