import axios from 'axios';
import {AUTH_SERVICE} from '../config';

export const makeRequest = async (method, url, data) => {
    console.log({
        method,
        url : `${AUTH_SERVICE}${url}`,
        data
    })
    return axios({
        method,
        url : `${AUTH_SERVICE}${url}`,
        data
    })
};