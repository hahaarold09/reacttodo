import React from 'react';
import { Form } from 'antd';
const { Item } = Form;

export default (props)=>{
    const newProps = Object.assign({}, props, {
        label : 'new label'
    });
    
    return (
        <Item {...newProps} >
            {props.children}
        </Item>
    );
}