import gql from "graphql-tag";

export const SUBSCRIPTIONS_TODOS = gql`
    subscription {
        todo {
            id
            name
        }
    }
`;
export const MUTATION_TODO = gql`
    mutation CreateTodo($name : String){
        insert_todo(objects: {name: $name}) {
            returning {
                id
            }
        }
    }
`;

export const DELETE_TODO = gql`
    mutation DeleteTodo($id : Int){
        delete_todo(where: {
            id : {
                _eq :$id
            }
        }) {
            returning {
                id
            }
        }
    }
`;

export const UPADTE_TODO = gql`
    mutation UpdateTodo($id : Int,$name : String){
        update_todo(
        _set: {name: $name}, 
        where: {id: {_eq: $id}
    }) {
        returning {
            id
        }
      }
   }
  
`;