import gql from "graphql-tag";

export const QUERY_USER = gql`
    query GetUser($username : String , $password : String){
        user(where:
             { 
              username : {_eq : $username },
              password : {_eq : $password } 
            }) {
                username
                password
        }
    }
`;
export const CHECK_USER = gql`
    query GetUser($username : String ){
        user(where:
             { 
              username : {_eq : $username }
            }) {
                id
        }
    }
`;
export const MUTATION_USER = gql`
    mutation InsertUser($username : String, $password : String){
        insert_user(objects: {
            username: $username,
            password: $password
        }) {
            returning {  
                 id
             }
          }
    }
`;
