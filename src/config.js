// export const GRAPHQL_WS = "ws://graphql-todo-postgres.herokuapp.com/v1/graphq"; // -> graphql.gideon.payruler.com
// export const GRAPHQL_HTTP = "https://graphql-todo-postgres.herokuapp.com/v1/graphql"; // -> graphql.gideon.payruler.com

export const GRAPHQL_WS = "ws://localhost:8080/v1/graphql"; // -> graphql.gideon.payruler.com
export const GRAPHQL_HTTP = "http://localhost:8080/v1/graphql"; // -> graphql.gideon.payruler.com
export const AUTH_SERVICE = "http://192.168.172.148:3001";
export const TOKEN = "payruler";

